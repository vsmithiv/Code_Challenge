﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BerwynCodeChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            List<String> guids = new List<String>();
            List<int> val1 = new List<int>();
            List<int> val2 = new List<int>();
            List<string> val3 = new List<string>();
            double avgVal3Length = 0;

            const bool IGNORE_FIRST_LINE = true; //set this to ignore first line of input (to deal with headers)
            const string OUTPUT_DELIM = ","; //set for whatever we want our output delimiter to be

            //initial parsing of input
            using (var reader = new StreamReader("test.csv"))
            {
                bool lineSkipped = false;
                while (!reader.EndOfStream)
                {
                    if (!lineSkipped && IGNORE_FIRST_LINE)
                    {
                        reader.ReadLine();
                        lineSkipped = true;
                    }

                    var line = reader.ReadLine();
                    var values = line.Split(", ");

                    guids.Add(values[0].Replace("\"", ""));
                    val1.Add(IntMaker(values[1]));
                    val2.Add(IntMaker(values[2]));
                    val3.Add(values[3].Replace("\"", ""));
                    
                }
            }

            //average val3 length
            foreach (string str in val3)
                avgVal3Length = avgVal3Length + str.Length;
            avgVal3Length = avgVal3Length / val3.Count;

            //built output set in memory
            String outputLine = "";
            string isDuplicateStr = "";
            string isLongerStr = "";
            List<string> lines = new List<string>();

            List<string> sortedGuids = new List<string>(guids);//create sorted copy of GUIDS list for faster dupe checking
            sortedGuids.Sort();

            for (int i = 0; i < guids.Count; i++)
            {
                //assign conditional flags
                if (IsDuplicateGUID(guids[i], sortedGuids))
                    isDuplicateStr = "Y";
                else
                    isDuplicateStr = "N";

                if (val3[i].Length > avgVal3Length)
                    isLongerStr = "Y";
                else
                    isLongerStr = "N";

                //"the meat" of the output
                outputLine = guids[i] + OUTPUT_DELIM +
                    (val1[i] + val2[i]).ToString() + OUTPUT_DELIM +
                    isDuplicateStr + OUTPUT_DELIM +
                    isLongerStr;
                lines.Add((string)outputLine.Clone());
            }

            //write it to a file
            using (var writer = new StreamWriter("output.csv"))
            {
                foreach (string line in lines)
                    writer.WriteLine(line);
                writer.Flush();
            }

        }

        //strip out formatting and convert to int
        static int IntMaker (string strVar)
        {
            string noQuotesStr = strVar.Replace("\"", "");

            if (Int32.TryParse(noQuotesStr, out int j)) { }
            else
                j = 0;

            return j;
        }

        //check if GUID is a dupe. Inputs a sorted list and 1 string to compare
        static bool IsDuplicateGUID(string guid, List<string> sortedGuids)
        {
            bool ret = false;
 
            int i = sortedGuids.IndexOf(guid);
            if (i < sortedGuids.Count - 1)
                if (guid.CompareTo(sortedGuids[i + 1]) == 0 )
                    ret = true;

            return ret;    
        }
    }
}
